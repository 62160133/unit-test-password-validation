
const { checkLength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has A alphabet in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has Z alphabet in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password Boy@12 to be false', () => {
    expect(checkPassword('Boy@12')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password to be true', () => {
    expect(checkDigit('1234567890')).toBe(true)
  })
})
